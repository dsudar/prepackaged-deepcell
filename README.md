# Prepackaged Deepcell for QiTissue

This is a package intended for installing Deepcell with QiTissue. The main purpose is to be able to install all dependencies easily and to provide an interface for QiTissue to use.

## Important Information
- **[Usage](docs/README.md)** -- Information on installing and using the package
- **[Development](docs/development.md)** -- Information on developing this package

Changelog can be found at [Changelog](docs/CHANGELOG.md)