# Prepackaged Deepcell for QiTissue

This is a package intended for installing Deepcell with QiTissue. The main purpose is to be able to install all dependencies easily and to provide an interface for QiTissue to use.

## Installation
This package can be installed using pip from the gitlab pypi registry
```bash
pip install prepacked-deepcell --index-url https://gitlab.com/api/v4/groups/claiv/-/packages/pypi/simple
```

## Testing
The package includes some tests that can be run to make sure the package is working properly on the  platform and environment.
```bash
pip install pytest
pytest --pyargs prepacked_deepcell
```
This will run some predictions on known data and compares them with the expected outcomes

## CLI
This package provides a cli that can be used via a CLI:

```bash
deepcell-cli -i path/to/input/file.npy -o file path/to/output/file.npy
```


Options:
```
  -h, --help            show this help message and exit
  -i IMAGE, --image IMAGE
                        input image path
  -c IMAGE_CYTO, --image_cyto IMAGE_CYTO
                        cytoplasm image path (for mesmer)
  -m {nuclear,cytoplasm,mesmer-whole-cell,mesmer-nuclear,mesmer-both}, --model {nuclear,cytoplasm,mesmer-whole-cell,mesmer-nuclear,mesmer-both}
                        the pretrained model to use
  -o OUTPUT, --output OUTPUT
                        the path to the output file. Supports numpy array (.npy) and tiff file (.tiff/.tif)
  -oc OUTPUT_CYTO, --output_cyto OUTPUT_CYTO
                        the path to the cyto label output file (for mesmer-both). Supports numpy array (.npy) and tiff file (.tiff/.tif)

```

Note that different models have different requirements, the mesmer models need two input images (nuclear + cytoplasm), mesmer-both also produces two segmentations (nuclear + cytoplasm) and needs two output paths

### Examples
- Segment a nuclear image using the default 'nuclear' model:
```bash
deepcell-cli -i input_image.npy -o output.tiff
```
- Segment a cytoplasm image using the 'mesmer-whole-cell' model:
plaintext
```bash
deepcell-cli -i input_image.npy -m mesmer-whole-cell -o output.tiff
```
- Segment both nucleus and cytoplasm using the 'mesmer-both' model:
```bash
deepcell-cli -i input_image.npy -c cytoplasm_image.npy -m mesmer-both -o output.tiff -oc output_cyto.tiff
```