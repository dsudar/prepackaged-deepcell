# [](https://gitlab.com/claiv/prepackaged-deepcell/compare/v0.6.1...v) (2023-10-30)



## [0.6.1](https://gitlab.com/claiv/prepackaged-deepcell/compare/v0.6.0...v0.6.1) (2023-10-30)


### Bug Fixes

* **cli:** add type float to image_mpp ([a05ca0c](https://gitlab.com/claiv/prepackaged-deepcell/commit/a05ca0cbdc8961909328a3634aceca7401a5c474))



# [0.6.0](https://gitlab.com/claiv/prepackaged-deepcell/compare/v0.5.4...v0.6.0) (2023-10-30)


### Bug Fixes

* **cli:** get image_mpp from args ([45683b9](https://gitlab.com/claiv/prepackaged-deepcell/commit/45683b9a8403bc80b4ffbddb4eb4012bd67918b9))


### Features

* **cli:** explicit gpu option ([248abb1](https://gitlab.com/claiv/prepackaged-deepcell/commit/248abb12a53e63884f2a129d93cafb47ebb09813))
* **prediction:** added option to set image per pixel for deepcell prediction ([d99867c](https://gitlab.com/claiv/prepackaged-deepcell/commit/d99867c55426fa9910fd25c41c4531ea332dc60e))



## [0.5.4](https://gitlab.com/claiv/prepackaged-deepcell/compare/v0.5.3...v0.5.4) (2023-10-05)


### Bug Fixes

* **dependency:** pin deepcell version to one that doesn't require authentication ([06e15d1](https://gitlab.com/claiv/prepackaged-deepcell/commit/06e15d1262b7c0a765305107012d9abe3ed2bdb1))



## [0.5.3](https://gitlab.com/claiv/prepackaged-deepcell/compare/v0.5.2...v0.5.3) (2023-09-29)


### Bug Fixes

* **cli:** swap order of predictions ([39bdcf2](https://gitlab.com/claiv/prepackaged-deepcell/commit/39bdcf25159e4200b6e28f0b89f77a81ef6092fc))



## [0.5.2](https://gitlab.com/claiv/prepackaged-deepcell/compare/v0.5.1...v0.5.2) (2023-09-29)


### Bug Fixes

* **cli:** wrong arg name fror cyto image ([c367d02](https://gitlab.com/claiv/prepackaged-deepcell/commit/c367d02223b03feedd013c22be0b196a4ded3ba4))



## [0.5.1](https://gitlab.com/claiv/prepackaged-deepcell/compare/v0.5.0...v0.5.1) (2023-09-29)


### Bug Fixes

* **cli:** check for output_cyto if mesmer-both model is selected ([46ad8e0](https://gitlab.com/claiv/prepackaged-deepcell/commit/46ad8e07ed3832f61784dd974d319dcba3a5887e))



# [0.5.0](https://gitlab.com/claiv/prepackaged-deepcell/compare/v0.4.2...v0.5.0) (2023-09-25)


### Features

* **cli:** support second cyto segmentation output for mesmer-both model ([663b073](https://gitlab.com/claiv/prepackaged-deepcell/commit/663b073a81113b50dd180fde244813378c610806))



## [0.4.2](https://gitlab.com/claiv/prepackaged-deepcell/compare/v0.4.1...v0.4.2) (2023-08-18)


### Bug Fixes

* **cli:** fix cli name, change to common deepcell-cli naming ([90fe0f9](https://gitlab.com/claiv/prepackaged-deepcell/commit/90fe0f9771178064c7e815a13b8ad3fc53b2a027))



## [0.4.1](https://gitlab.com/claiv/prepackaged-deepcell/compare/v0.4.0...v0.4.1) (2023-07-07)


### Bug Fixes

* **dependency:** add explicit url for toolbox ([01543d1](https://gitlab.com/claiv/prepackaged-deepcell/commit/01543d1b7c2bc55ef170d81be97fb9fd23533fbb))



# [0.4.0](https://gitlab.com/claiv/prepackaged-deepcell/compare/e3d371c5c2d3c8ef4010eaac737269487e940d49...v0.4.0) (2023-07-07)


### Bug Fixes

* **build:** package name ([1df9fd3](https://gitlab.com/claiv/prepackaged-deepcell/commit/1df9fd32b360801e420f187985db9847ebf3a868))
* default None for compartment ([174d4ea](https://gitlab.com/claiv/prepackaged-deepcell/commit/174d4eacbbc2d0104a88d3ace0d43ed82037ace7))
* **predict:** concatenate instead of stack for two input images ([7e1998e](https://gitlab.com/claiv/prepackaged-deepcell/commit/7e1998ecfa85929dea832e8c6e879b853b528a39))
* **predict:** squeeze prediction to proper dimension ([48b6c90](https://gitlab.com/claiv/prepackaged-deepcell/commit/48b6c90efebd688fec627113cd542c05e97af816))


### Features

* add different model options from deepcell ([8c13b5c](https://gitlab.com/claiv/prepackaged-deepcell/commit/8c13b5cefc74e24a3ed71b914d5406a0e0abedde))
* **cli:** add basic cli ([d348cb2](https://gitlab.com/claiv/prepackaged-deepcell/commit/d348cb260c1588ad25b88db083f0e99c021531d4))
* initial deepcell implementation using NuclearSegmentation, simple test ([e3d371c](https://gitlab.com/claiv/prepackaged-deepcell/commit/e3d371c5c2d3c8ef4010eaac737269487e940d49))
* **predict:** add functionality to pass two input images for Mesmer model ([7cb9da3](https://gitlab.com/claiv/prepackaged-deepcell/commit/7cb9da39f18f8f8a83d120c2d7a99e3c9e724b70))



