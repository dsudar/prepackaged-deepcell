from pathlib import Path
from typing import ContextManager

import pytest
import importlib.resources
from .. import data
import numpy as np


@pytest.fixture
def ex_input_data_path() -> 'ContextManager[Path]':
    return importlib.resources.path(data, 'test_input.npy')


@pytest.fixture
def ex_input_data_path_2() -> 'ContextManager[Path]':
    return importlib.resources.path(data, 'test_input.npy')


@pytest.fixture
def ex_prediction_output() -> np.ndarray[(int, int)]:
    with importlib.resources.open_binary(data, "test_output.npy") as f:
        example_prediction = np.load(f)
    assert example_prediction.ndim == 2
    return example_prediction


@pytest.fixture
def ex_prediction_output_mesmer_both() -> np.ndarray[(int, int)]:
    with importlib.resources.open_binary(data, "test_output_mesmer_both_gpu.npy") as f:
        example_prediction = np.load(f)
    assert example_prediction.ndim == 2
    return example_prediction


@pytest.fixture
def ex_prediction_output_mesmer_both_cyto() -> np.ndarray[(int, int)]:
    with importlib.resources.open_binary(data, "test_output_mesmer_both_cyto_gpu.npy") as f:
        example_prediction = np.load(f)
    assert example_prediction.ndim == 2
    return example_prediction


@pytest.fixture
def ex_prediction_input() -> np.ndarray[(int, int)]:
    with importlib.resources.open_binary(data, "test_input.npy") as f:
        example_prediction = np.load(f)
    assert example_prediction.ndim == 2
    return example_prediction
