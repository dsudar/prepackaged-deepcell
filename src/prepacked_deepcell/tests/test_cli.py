import pathlib
from pathlib import Path
from typing import ContextManager

import pytest
from .fixtures.data import ex_prediction_output, ex_input_data_path, ex_input_data_path_2, \
    ex_prediction_output_mesmer_both, ex_prediction_output_mesmer_both_cyto
import subprocess
import numpy as np
import os
import tifffile as tiff
import logging


@pytest.mark.usefixtures('ex_prediction_output', 'ex_input_data_path')
def test_prediction_default(ex_prediction_output, ex_input_data_path):
    ex_output_data_path = './ex_output_data.npy'
    run_deepcell(ex_input_data_path, ex_output_data_path)

    labels = np.load(ex_output_data_path)

    assert np.array_equal(labels, ex_prediction_output)

    os.remove(ex_output_data_path)


@pytest.mark.usefixtures('ex_prediction_output', 'ex_input_data_path')
def test_prediction_default_tiff(ex_prediction_output, ex_input_data_path):
    ex_output_data_path = './ex_output_data.tiff'
    run_deepcell(ex_input_data_path, ex_output_data_path)

    labels = np.array(tiff.imread(ex_output_data_path))

    assert np.array_equal(labels, ex_prediction_output)

    os.remove(ex_output_data_path)


@pytest.mark.usefixtures('ex_prediction_output_mesmer_both', 'ex_prediction_output_mesmer_both_cyto',
                         'ex_input_data_path', 'ex_input_data_path_2')
def test_mesmer_both_gpu(ex_prediction_output_mesmer_both, ex_prediction_output_mesmer_both_cyto, ex_input_data_path,
                     ex_input_data_path_2):
    ex_output_data_path = './ex_output_data.tiff'
    ex_output_data_path_cyto = './ex_output_data_cyto.tiff'

    run_deepcell(ex_input_data_path, ex_output_data_path,
                 input_path_cyto=ex_input_data_path_2, output_path_cyto=ex_output_data_path_cyto,
                 extra_options=['-m', 'mesmer-both'], gpu=True)

    labels = np.array(tiff.imread(ex_output_data_path))
    labels_cyto = np.array(tiff.imread(ex_output_data_path_cyto))

    assert np.array_equal(labels, ex_prediction_output_mesmer_both)
    assert np.array_equal(labels_cyto, ex_prediction_output_mesmer_both_cyto)

    os.remove(ex_output_data_path)
    os.remove(ex_output_data_path_cyto)


def run_deepcell(input_path: ContextManager[Path], output_path: str, extra_options: list[str] = [],
                 input_path_cyto: ContextManager[Path] = None, output_path_cyto: str = None, gpu=False):
    with input_path as path:

        cmd = ['deepcell-cli', '-i', str(path.absolute()), '-o', output_path]

        if not gpu:
            cmd.extend([ '--gpu', 'no']);

        if input_path_cyto is not None:
            with input_path_cyto as cyto_path:
                cmd.extend(['-c', str(cyto_path.absolute())])
        if output_path_cyto is not None:
            cmd.extend(['-oc', output_path_cyto])

        cmd.extend(extra_options)

        logging.info(f'running command {" ".join(cmd)}')

        result = subprocess.run(
            " ".join(cmd),
            capture_output=True, text=True, shell=True
        )
        logging.info(result.stdout)
        if result.stderr != '':
            logging.error(result.stderr)
