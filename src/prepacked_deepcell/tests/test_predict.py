import pytest
import numpy as np
from prepacked_deepcell import DeepCell
from .fixtures.data import ex_prediction_output, ex_prediction_input


def test_prediction_default(ex_prediction_input, ex_prediction_output):
    algorithm = DeepCell(use_gpu='no')

    labels = algorithm.predict(ex_prediction_input)

    assert np.array_equal(labels, ex_prediction_output)