import argparse
import logging
import numpy as np
from prepacked_deepcell import DeepCell
import tifffile as tiff


class DeepCellCLI:
    def __init__(self):
        logging.getLogger().setLevel(logging.INFO)

        self.parser = argparse.ArgumentParser(description='DeepCell CLI')
        self.parser.add_argument('-i', '--image', help='input image path', required=True)
        self.parser.add_argument('-c', '--image_cyto', help='cytoplasm image path (for mesmer)')
        self.parser.add_argument('-m', '--model', help='the pretrained model to use',
                                 choices=['nuclear', 'cytoplasm', 'mesmer-whole-cell', 'mesmer-nuclear', 'mesmer-both'], default='nuclear')
        self.parser.add_argument('-mpp', '--image_mpp', type=float, help='microns per pixel for image', default= None)
        self.parser.add_argument('-o', '--output', help='the path to the output file. Supports numpy array (.npy) and tiff file (.tiff/.tif)', required=True)
        self.parser.add_argument('-oc', '--output_cyto', help='the path to the cyto label output file (for mesmer-both). Supports numpy array (.npy) and tiff file (.tiff/.tif)')
        self.parser.add_argument('-g', '--gpu', help='if gpu should be used, defaults to auto', choices=['auto', 'no'],
                            default='auto')

    def run(self):
        args = self.parser.parse_args()

        if args.model == 'mesmer-both':
            assert args.output_cyto is not None, 'Please provide --output_cyto for mesmer-both model'

        logging.info('Initializing DeepCell')
        deepcell = DeepCell(model=args.model, use_gpu=args.gpu)

        image = np.load(args.image)
        image_cyto = None
        if args.image_cyto:
            image_cyto = np.load(args.image_cyto)

        logging.info('Running DeepCell prediction')
        prediction = deepcell.predict(image=image, image2=image_cyto, image_mpp=args.image_mpp)

        if args.model == 'mesmer-both':
            # first cell then nucleus
            # (https://deepcell.readthedocs.io/en/latest/_modules/deepcell/applications/mesmer.html)
            #    label_images = np.concatenate([
            #     label_images_cell,
            #     label_images_nucleus
            # ], axis=-1)

            prediction_cyto = prediction[..., 0]
            prediction = prediction[..., 1]

            if args.output_cyto.endswith('.tif') or args.output_cyto.endswith('.tiff'):
                tiff.imsave(args.output_cyto, prediction_cyto.astype(np.uint16))
                logging.info(f'Prediction saved as {args.output_cyto} (TIFF format)')
            else:
                np.save(args.output_cyto, prediction_cyto)
                logging.info(f'Prediction saved as {args.output_cyto} (NumPy .npy format)')


        if args.output.endswith('.tif') or args.output.endswith('.tiff'):
            tiff.imsave(args.output, prediction.astype(np.uint16))
            logging.info(f'Prediction saved as {args.output} (TIFF format)')
        else:
            np.save(args.output, prediction)
            logging.info(f'Prediction saved as {args.output} (NumPy .npy format)')


def cli():
    DeepCellCLI().run()
