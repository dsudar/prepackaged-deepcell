import os
from typing import Literal
import numpy as np
from deepcell.applications import NuclearSegmentation, CytoplasmSegmentation, Mesmer


class DeepCell:
    def __init__(self, model: Literal[
        'nuclear', 'cytoplasm', 'mesmer-whole-cell', 'mesmer-nuclear', 'mesmer-both'] = 'nuclear',
                 use_gpu: Literal['auto', 'no'] = 'auto'):
        """
        :param model: the pre-trained model to use. Options are: 'nuclear', 'cytoplasm', 'mesmer-whole-cell', 'mesmer-nuclear', 'mesmer-both'.
                      Defaults to 'nuclear'. see https://deepcell.readthedocs.io/en/master/API/deepcell.applications.html#application for info
        """

        if use_gpu == 'no':
            os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

        self.compartment = None

        if model == 'nuclear':
            self.model = NuclearSegmentation()
        elif model == 'cytoplasm':
            self.model = CytoplasmSegmentation()
        elif model.startswith('mesmer'):
            self.model = Mesmer()
            self.compartment = model.split("-", 1)[-1]
        else:
            self.model = NuclearSegmentation()

    def predict(self, image: np.ndarray[(np.uint16, np.uint16)], image2: np.ndarray[(np.uint16, np.uint16)] = None,
                image_mpp: float = None) -> np.ndarray[(int, int)]:
        """
        :param image: input image (2d greyscale image)
        :param image2: input image2, used depending on algorithm (2d greyscale image)
        :param image_mpp: mircons per pixel for the image
        :return: the prediction mask as numpy array
        """

        expanded_input = np.expand_dims(image, axis=(0, 3))

        if image2 is not None:
            expanded_input2 = np.expand_dims(image2, axis=(0, 3))
            expanded_input = np.concatenate((expanded_input, expanded_input2), axis=3)

        if self.compartment is not None:
            assert image2 is not None, "For Mesmer two images are needed: nucleus + cytoplasm"

            # whole cell:
            # one output image shape (1, x, y, 1)
            # both:
            # two output images shape (1, x, y, 2) first cell then nucleus
            # nuclear:
            # one output image shape (1, x, y, 1)

            labels = self.model.predict(expanded_input, image_mpp=image_mpp, compartment=self.compartment)
        else:
            assert image2 is None, "For this model only one input image is accepted"

            labels = self.model.predict(expanded_input, image_mpp=image_mpp)

        return labels.squeeze()
